#!/usr/bin/php
<?php

include("functions.php");

#list($me, $groupid, $path) = $argv;


## MAIN
#
#

$now = get_now();
$groups = get_groups();

print "Now: {$now}\n";

#exit;

print "Found Groups:\n";
if ($groups) {
    foreach($groups as $group) {
        printf("%3d  %20s  %2d  %d\n",$group['id'],$group['name'],$group['preferred'], $group['autoscan']);
    }
}
print "\n";

if (!$path) {
	foreach ($groups as $group) {
		if ($group['autoscan'] == true) {
			print "Processing {$group['name']}...\n";
			$stats = array(
				'count' => 0,
				'found' => 0,
				'added' => 0,
				'skipped' => 0,
				'removed' => 0,
				'replaced' => 0,
				'error' => 0);

			if ($ret = scanDirs($group, $files, $group['root'])) {
				print "### Pruning old files in [" . $group['root'] . "] from DB.\n";
				prune_old($group['id'],$now);
			}


			print "Count:    {$stats['count']}\n";
			print "Found:    {$stats['found']}\n";
			print "Added:    {$stats['added']}\n";
			print "Skipped:  {$stats['skipped']}\n";
			print "Removed:  {$stats['removed']}\n";
			print "Replaced: {$stats['replaced']}\n";
			print "\n";

		} else {
			print "Skipping {$group['name']}.\n";
		}
	}
} else {

	#print_r($group);

	#$groupid = strval($group);
	print "Scanning group " . $groups[$groupid]['name'] . "\n";
	print "Location: " . $groups[$groupid]['root'] . $path . "\n";

	$stats = array(
		'count' => 0,
		'found' => 0,
		'added' => 0,
		'skipped' => 0,
		'removed' => 0,
		'replaced' => 0,
		'error' => 0);

	scanDirs($groups[$groupid], $files, $groups[$groupid]['root'] . $path);
	
	print "Count:    {$stats['count']}\n";
	print "Found:    {$stats['found']}\n";
	print "Added:    {$stats['added']}\n";
	print "Skipped:  {$stats['skipped']}\n";
	print "Removed:  {$stats['removed']}\n";
	print "Replaced: {$stats['replaced']}\n";
	print "\n";

}

?>
