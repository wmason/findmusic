<?php

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

include("functions.php");

$s = '';

if (isset($_GET['s']) && strlen($_GET['s']) > 0) {
	$s = urldecode($_GET['s']);
}

$url = "http://contag.us";

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html, charset=utf-8" />
    <meta name="viewport" content="width=screen.width; initial-scale=1" />

	<link rel="stylesheet" href="themes/findmusic1.min.css" />
	<link rel="stylesheet" href="themes/jquery.mobile.icons.min.css" />
	<link rel="stylesheet" href="js/jquery.mobile.structure-1.4.5.min.css" /> 
	<script src="js/jquery-1.11.1.min.js"></script> 
    <script>
    $(document).bind('mobileinit', function () {
        $.mobile.activeBtnClass = 'unused';
    });
    </script>
	<script src="js/jquery.mobile-1.4.5.min.js"></script> 

</head>
<body>
	<div data-role="page" data-theme="a" class="ui-responsive-panel">
		<div data-role="header">
			<form name="searchbox" id="searchbox" method="get">
				<div data-role="navbar" data-theme="c">
					<ul>
						<li><input id="search" type="text" name="s" value="<?php print $s; ?>" /></li>
					</ul>
					<ul>
						<li><a href="#" data-role="button" data-theme="c" onclick="$(this).closest('form').submit()">Search</a></li>
<!-- <li><a href="#" data-role="button"><s>Play All</s></a></li> --!>
						<li><a href="index.php?s=<?php print urlencode($s); ?>" target="_top" data-ajax="false" data-role="button">Update URL</a></li>
						<li><a href="?s=" data-role="button" data-iconpos="right" >Reset</a></li>
					</ul>
				</div>
			</form>
		</div>

<?php

	if (strlen($s) > 0) {
		$songs = find_all_files($s);
		if (isset($songs) && count($songs) > 0) {
			print "<div data-role=\"content\">";
			$div = '';
			$path = '';
			$count1 = 0;

			foreach ($songs as $group) {
				$out = '';

				foreach ($group['files'] as $path => $files) {
					$count2 = count($files);
					$count1 += $count2;

					$out .= "<div data-role=\"collapsible\" data-collapsed=\"false\" data-theme=\"b\" data-inset=\"true\" data-collapsed-icon=\"carat-d\" data-expanded-icon=\"grid\">";
					$out .= "<h4>" . $path . "<span class=\"ui-li-count\">$count2</span></h4>";
					$out .= "<ul data-role=\"listview\" data-theme=\"a\">";
					$out .= "<li data-mini=\"true\" data-icon=\"false\">";
					$out .= "<a href=\"?s=" . urlencode($group['root'] . '/' . $path) . "\" style=\"font-size:10px; text-align: center;\">THIS ALBUM ONLY</a>";
					$out .= "</li>";
					
					foreach ($files as $file) {
						$out .= "<li data-mini=\"true\" data-icon=\"false\">";
						$out .= "<a target=\"player\" href=\"playmp3.php?id={$file['id']}\" data-mini=\"true\">";
						$out .= "<img src=\"images/Play1Normal.png\" class=\"ui-li-icon ui-corner-none\">";
						$out .= $file['filename'];
						$out .= "</a>";
						$out .= "</li>";
					}
					$out .= "</ul>";
					$out .= "</div>";

				}

				if ($group['preferred'] == 0) {
					$collapsed = 'true';
				} else {
					$collapsed = 'false';
				}

				print "<div data-role=\"collapsible\" data-collapsed=\"$collapsed\" data-theme=\"c\" data-inset=\"false\">";
				print "<h4>" . strtoupper($group['name']) . "<span class=\"ui-li-count\">$count1</span></h4>";
				print "<ul data-role=\"listview\" data-theme=\"a\">";

				print $out;

				print "</ul>";
				print "</div>";
			}

			if ($count1 == 0) {
				print "<div data-role=\"content\">";
				print "<p>No results found.</p>";
				print "</div>";
			}

		} else {
			
			print "<div data-role=\"content\">";
			print "<p>No results found.</p>";
			print "</div>";
		}
	} else {
		print "<div data-role=\"content\">";
		print "<p>Enter text or words contained in the Artist or Title of the song you are looking for seperated with spaces.  Words don't need to be in and order.</p>";
		print "</div>";
	}


$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);

print "</div> <!-- /page -->";
print "<div data-role=\"footer\">";
print '<p>Page generated in '.$total_time.' seconds.</p>';
print "</div>";

?>
</body>
</html>
