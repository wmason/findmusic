<?php 
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$url="http://contag.us";
$theme="test3.css";
?>
<html> 
	<head> 
	<title>ConTag</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="<?php print $url; ?>/includes/jquery.mobile-1.4.5.min.css" />
	<link rel="stylesheet" href="<?php print $url . $theme; ?>" />
	<link rel="stylesheet" href="<?php print $url; ?>/includes/custom.css" />
	<script src="<?php print $url; ?>/includes/jquery-1.7.1.min.js"></script>
	<script src="<?php print $url; ?>/includes/jquery.mobile-1.4.5.min.js"></script>
<style type="text/css">
#tick{display:none}
#cross{display:none}
#pwcross{display:none}
#pwlen{display:none}
#emcross{display:none}
</style>

</head> 
<body> 

<div data-role="page" data-theme="<?php print $sidetheme; ?>" data-dom-cache="false" <?php if ($page_class) { print "class=\"$page_class\""; } ?> class="ui-responsive-panel">

<a href="#" data-role="button">Link button</a>
<form>
    <button>Button</button>
    <input type="button" value="Input button" />
    <input type="submit" value="Submit button" />
    <input type="reset" value="Reset button" />
</form>

<a href="#" data-role="button" data-inline="true">First button</a>
<a href="#" data-role="button" data-inline="true" data-theme="b">Second button</a>
<a href="#" data-role="button" data-inline="true" data-theme="a" data-mini="true">Mini button</a>

<a href="#" data-role="button" data-icon="star" data-theme="b">Icon button</a>
<a href="#" data-role="button" data-icon="star" data-theme="b" data-iconpos="top">Icon button</a>
<a href="#" data-role="button" data-icon="star" data-theme="b" data-iconpos="bottom">Icon button</a>
<a href="#" data-role="button" data-icon="star" data-theme="b" data-iconpos="right">Icon button</a>

<a href="#" data-role="button" data-icon="star" data-iconpos="notext" data-theme="b">Icon button</a>

</div>
</body>
