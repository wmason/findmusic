#!/usr/bin/php
<?php

include("config.php");

#$types = array(	'mp3' => 'audio/mpeg', 'mp4' => 'audio/mp4', 'ogg' => 'audio/ogg', 'flac' => 'audio/flac', 'm4a' => 'audio/mp4' );

$types = array(	'mp3', 'mp4', 'ogg', 'flac', 'm4a');


// //////////////////////////////////////////////////////
// Open MySQL Connection
mysql_connect("$host", "$username", "$password") or die;
mysql_select_db("$db_name") or die;


##
## DB Functions
##

function get_groups() {
    $sql = "SELECT * FROM groups";
    if ($result = mysql_query($sql)) {
        while ($group = mysql_fetch_assoc($result)) {
            $groups[$group['id']] = $group;
        }
        return $groups;

    } else {
        print mysql_error() . "\n";
        return false;

    }
}

function scanDirs($group, &$myfiles, $target) {

    #$files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

	if ($files = scandir($target)) {
		print_r($files);
		foreach( $files as $file ) {
			if ($file != '.' && $file != '..') {
				$file = $target . '/' . $file;
				if(is_dir($file)){
					print "Dir: $file\n";
					scanDirs($group, $myfiles, $file);
				} elseif (is_file($file)) {
					print "File: $file\n";
				} else {
					print "Not File/Dir: $file\n";
				}
			}
		}
	} else {
		print "ERROR: Bad directory $target\n";
	}
}

function process($group, $file) {

    global $stats;

    $pathinfo = pathinfo($file);
    $full_file = $pathinfo['filename'] . '.' . $pathinfo['extension'];
    $stat = stat($file);
    $file_size = $stat[7];
	$stats['count']++;

    if (!find_file($group, $pathinfo, $stat)) {
        store_file($group, $pathinfo, $stat, $file);
    } else {
		$stats['found']++;
	}

    printf("[C:%d/F:%d/A:%d/S:%d/E:%d] Found file %s [%d]\n", $stats['count'], $stats['found'], $stats['added'], $stats['skipped'], $stats['error'], $file, $file_size);
}

function find_file($group, $pathinfo, $stat) {

	$filename = mysql_real_escape_string($pathinfo['filename'] . '.' . $pathinfo['extension']);
	$filesize = mysql_real_escape_string($stat[7]);
	$pathname = mysql_real_escape_string(rem_group_path($group['root'], $pathinfo['dirname']));
	$groupid = mysql_real_escape_string($group['id']);
	
	$sql = "SELECT * FROM files f LEFT JOIN groups g ON f.groupid = g.id  WHERE f.filename = '$filename' AND path = '$pathname' AND f.groupid = $groupid ORDER BY g.preferred DESC, f.id";

	if ($result = mysql_query($sql)) {
		if (mysql_num_rows($result) == 0) {
			return false;
		} else {
			while (($files[] = mysql_fetch_assoc($result)) || array_pop($files));
			return $files;
		}
	} else {
		print "ERROR: Failed on [$sql] with [". mysql_error() ."]\n";
		exit(10);
	}

}

function find_file_md5($group, $pathinfo, $stat, $md5) {

	$filename = mysql_real_escape_string($pathinfo['filename'] . '.' . $pathinfo['extension']);
	$filesize = mysql_real_escape_string($stat[7]);
	$md5 = mysql_real_escape_string($md5);
	
	$sql = "SELECT * FROM files f LEFT JOIN groups g ON f.groupid = g.id  WHERE f.md5 = '$md5' AND f.size = $filesize ORDER BY g.preferred DESC, f.id";

	if ($result = mysql_query($sql)) {
		if (mysql_num_rows($result) == 0) {
			return false;
		} else {
			while (($files[] = mysql_fetch_assoc($result)) || array_pop($files));
			return $files;
		}
	} else {
		print "ERROR: Failed on [$sql] with [". mysql_error() ."]\n";
		exit(10);
	}

}
function store_file($group, $pathinfo, $stat, $file) {

	global $stats, $types;

	$filename = mysql_real_escape_string($pathinfo['filename'] . '.' . $pathinfo['extension']);
	$extension = mysql_real_escape_string($pathinfo['extension']);
	$filesize = mysql_real_escape_string($stat[7]);
	$pathname = mysql_real_escape_string(rem_group_path($group['root'], $pathinfo['dirname']));
	$groupid = mysql_real_escape_string($group['id']);
    $md5 = md5_file($file);

	if (in_array(strtolower($extension), $types)) {
		$sql = "INSERT INTO files VALUES (null, '$filename', '$extension', '$pathname', $groupid, '$md5', $filesize, 0)";
		if ($result = mysql_query($sql)) {
			$stats['added']++;
			return true;
		} else {
			$stats['error']++;
			print "ERROR: Failed to store file [$sql] with [" . mysql_error() . "]\n";
			return false;
		}
	} else {
		$stats['skipped']++;
	}

}

function rem_group_path($grouproot, $dirname) {

	if ($grouproot == substr($dirname, 0, strlen($grouproot))) {
		return substr($dirname, strlen($grouproot) + 1);
	} else {
		print "     GR: $grouproot\n";
		print "     DN: " . substr($dirname, 0, strlen($grouproot)) . "\n";
		return $dirname;
	}

	return false;
}

?>
<?php

## MAIN
#
#

$groups = get_groups();

print "Found Groups:\n";
if ($groups) {
    foreach($groups as $group) {
        printf("%20s  %2d  %d\n",$group['name'],$group['preferred'], $group['autoscan']);
    }
}
print "\n";

scanDirs(null,$files, '/smb/dad10/Music/_Done/New Order/');

?>
