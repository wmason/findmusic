<?php

include("config.php");

#$types = array(	'mp3' => 'audio/mpeg', 'mp4' => 'audio/mp4', 'ogg' => 'audio/ogg', 'flac' => 'audio/flac', 'm4a' => 'audio/mp4' );

$types = array(	'mp3', 'mp4', 'ogg', 'flac', 'm4a');


// //////////////////////////////////////////////////////
// Open MySQL Connection
$dbh = mysqli_connect("$host", "$username", "$password", "$db_name") or die;


##
## DB Functions
##

function get_groups() {
    global $dbh;
    $groups = array();
    $sql = "SELECT * FROM groups ORDER BY preferred DESC, id ASC";
    if ($result = mysqli_query($dbh, $sql)) {
        while ($group = mysqli_fetch_assoc($result)) {
            $groups[$group['id']] = $group;
        }
        return $groups;

    } else {
        print mysqli_error($dbh) . "\n";
        return false;

    }
}

function scanDirs($group, &$myfiles, $target) {

	print "[{$group['name']}] Scanning Directory: " . $target . "\n";

    #$files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

	if ($target == '.' || $target == '..') {
			print " - Skipped\n";
			return false;
	}

	if ($files = scandir($target)) {
		#print_r($files);
		foreach( $files as $file ) {
			if ($file != '.' && $file != '..') {
				$file = $target . $file;
				if(is_dir($file)){
					scanDirs($group, $myfiles, $file . '/');
				} elseif (is_file($file)) {
					process($group, $file);
				}
			}
		}
		return true;
	} else {
		print "ERROR: Bad directory $target\n";
		return false;
	}
}

function process($group, $file) {

    global $stats;

    $pathinfo = pathinfo($file);
    $full_file = $pathinfo['filename'] . '.' . $pathinfo['extension'];
    $stat = stat($file);
    $file_size = $stat[7];
	$stats['count']++;

    if (!find_file($group, $pathinfo, $stat)) {
        store_file($group, $pathinfo, $stat, $file);
    } else {
		$stats['found']++;
	}

    printf("[C:%d/F:%d/A:%d/S:%d/E:%d] Found file %s [%d]\n", $stats['count'], $stats['found'], $stats['added'], $stats['skipped'], $stats['error'], $file, $file_size);
}

function find_file($group, $pathinfo, $stat) {
	global $dbh;

	$filename = mysqli_real_escape_string($dbh, $pathinfo['filename'] . '.' . $pathinfo['extension']);
	$filesize = mysqli_real_escape_string($dbh, $stat[7]);
	$pathname = mysqli_real_escape_string($dbh, rem_group_path($group['root'], $pathinfo['dirname']));
	$groupid = mysqli_real_escape_string($dbh, $group['id']);
	
	$sql = "SELECT * FROM files f LEFT JOIN groups g ON f.groupid = g.id  WHERE f.filename = '$filename' AND path = '$pathname' AND f.groupid = $groupid ORDER BY g.preferred DESC, f.id";

	if ($result = mysqli_query($dbh, $sql)) {
		if (mysqli_num_rows($result) == 0) {
			return false;
		} else {
			while (($files[] = mysqli_fetch_assoc($result)) || array_pop($files));
			return $files;
		}
	} else {
		print "ERROR: Failed on [$sql] with [". mysqli_error($dbh) ."]\n";
		exit(10);
	}

}

function find_file_md5($group, $pathinfo, $stat, $md5) {

	$filename = mysqli_real_escape_string($dbh, $pathinfo['filename'] . '.' . $pathinfo['extension']);
	$filesize = mysqli_real_escape_string($dbh, $stat[7]);
	$md5 = mysqli_real_escape_string($dbh, $md5);
	
	$sql = "SELECT * FROM files f LEFT JOIN groups g ON f.groupid = g.id  WHERE f.md5 = '$md5' AND f.size = $filesize ORDER BY g.preferred DESC, f.id";

	if ($result = mysqli_query($dbh, $sql)) {
		if (mysqli_num_rows($result) == 0) {
			return false;
		} else {
			while (($files[] = mysqli_fetch_assoc($result)) || array_pop($files));
			return $files;
		}
	} else {
		print "ERROR: Failed on [$sql] with [". mysqli_error($dbh) ."]\n";
		exit(10);
	}

}
function store_file($group, $pathinfo, $stat, $file) {

	global $stats, $types, $dbh;

	$filename = mysqli_real_escape_string($dbh, $pathinfo['filename'] . '.' . $pathinfo['extension']);
	$extension = mysqli_real_escape_string($dbh, $pathinfo['extension']);
	$filesize = mysqli_real_escape_string($dbh, $stat[7]);
	$pathname = mysqli_real_escape_string($dbh, rem_group_path($group['root'], $pathinfo['dirname']));
	$groupid = mysqli_real_escape_string($dbh, $group['id']);
    $md5 = md5_file($file);

	if (in_array(strtolower($extension), $types)) {
		$sql = "INSERT INTO files VALUES (null, '$filename', '$extension', '$pathname', null, $groupid, '$md5', $filesize, 0, now())";
		if ($result = mysqli_query($dbh, $sql)) {
			$stats['added']++;
			return true;
		} else {
			$stats['error']++;
			print "ERROR: Failed to store file [$sql] with [" . mysqli_error($dbh) . "]\n";
			return false;
		}
	} else {
		$stats['skipped']++;
	}

}

function get_now() {
	global $dbh;

	$sql = "SELECT now()";
	if ($results = mysqli_query($dbh, $sql)) {
		$now_array = mysqli_fetch_row($results);
		$now = array_shift($now_array);
		return $now;
	} else {
		return false;
	}
}

function purge_old($group,$mynow) {

	$sql = "DELETE FROM files WHERE groupid = $group AND touched <= $mynow";
	if ($result = mysqli_query($dbh, $sql)) {
		return true;
	} else {
		print "ERROR: Failed to prune old files [$sql] with [" . mysqli_error($dbh) . "]\n";
		return false;
	}

}

function find_all_files($string) {

	$groups = get_groups();
	$items = preg_split("/ /", $string);
	$found = array();

	foreach ($groups as $group) {
		if ($files = find_group_files($group, $items)) {
			$group['files'] = $files;
			$found[] = $group;
		}
	}

	if (count($found) > 0) {
		return $found;
	}
	return false;
}		


function find_group_files($group, $items) {

	global $dbh;

	foreach($items as $item) {
		$query[] = "fqfilename LIKE '%" . mysqli_real_escape_string($dbh, $item) . "%'";
	}

	$files = array();

	$sql = "SELECT * FROM search1 WHERE groupname = '" . mysqli_real_escape_string($dbh, $group['name']) . "' AND " . implode(" AND ", $query) . " ORDER BY preferred DESC, fqfilename ASC LIMIT 300";

	if ($result = mysqli_query($dbh, $sql)) {
		while($song = mysqli_fetch_assoc($result)) {
			$files[$song['path']][] = $song;
		}
	}

	if (count($files) > 0) {
		return $files;
	}
	return false;

}


function rem_group_path($grouproot, $dirname) {

	if ($grouproot == substr($dirname, 0, strlen($grouproot))) {
		return substr($dirname, strlen($grouproot));
	} else {
		print "     GR: $grouproot\n";
		print "     DN: " . substr($dirname, 0, strlen($grouproot)) . "\n";
		return $dirname;
	}

	return false;
}

?>
