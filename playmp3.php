<?php


include("functions.php");

if(isset($_GET['id'])) {

	$id = mysqli_real_escape_string($dbh, $_GET['id']);
	$sql = "SELECT * FROM search1 WHERE id = $id";
	if ($result = mysqli_query($dbh, $sql)) {
		$rec = mysqli_fetch_assoc($result);
		

		if (file_exists($rec['fqfilename'])) {
			header('Content-Type: audio/mpeg');
			header('Content-Disposition: inline;filename="' . $rec['filename'] . '"');
			header('Content-length: '.filesize($rec['fqfilename']));
			header('Cache-Control: no-cache');
			header("Content-Transfer-Encoding: chunked");

			readfile($rec['fqfilename']);
			exit;
		} else {
			print "File Not Found: " . $rec['fqfilename'];
		}
	}
}

print_r($rec);

#header("HTTP/1.0 404 Not Found");

?>
