# README #

'FindMusic', a simple(ish) web page to look for music you own on your PC or accessible drive.  The interface is simple, letting you search for words or text stored in paths or filenames of the files you are looking for.  They don't even need to be in a specific order.  Then, once you find what you want, you can click the file to begin playing it.

### Features ###

* free form text searching.  'Leopard Def' brings up 'Def Leopard'
* Supports mp3s only, no transcoding yet.
* Multiple 'repositories' of your music for scanning, along with a 'preferred' option so certain repos will show up first if they match.


### Requirements ###

* MySQL database
* PHP5 enabled webserver
* PHP5 CLI for running scans
* Access to your mp3 files on the host with the webserver

(Note: I am currently testing this on a Raspberry Pi3 with MySQL, Apache and PHP5 with no issues.)


### Planned Features ###

* Transcoding of non-MP3s to mp3 on the fly.
* replacing the in-browser player with jplayer or other HTML5 player (TBD).
* Playing multiple files based on the search.
* Caching any transcoded files for immediate replay.
* Cleanup process to remove any old Cached files (to be kicked off via cron and/or page call) based on age or over max cache size.
* Download file Option.


# Coming Soon #

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact